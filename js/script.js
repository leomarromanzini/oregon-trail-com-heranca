class Traveler {
    constructor (name) {
        
        this._name = name;
        this.food = 1;
        this._isHealthy = true;
    }

    get name () {
       return ` My name is ${this._name}`;
    }

    set name(newName) {
        if(typeof newName !== "string"){
            return
        }else{
            return this._name = newName;
        }
        
        
    }

    get food () {
        return this._food;
    }

    set food (newFood) {

        if(newFood < 0){
            return this._food = 0;
        }else{
            return this._food = newFood;
        }
        
    }


    get isHealthy () {
        return this._isHealthy;
    }

    set isHealthy (newHealth) {
        return this._isHealthy = newHealth
    }

    hunt() {
        this.food += 2;
    }
    

    eat() {
        this.food > 0 ? this.food -= 1 : this.isHealthy = false;
    }
}

class Doctor extends Traveler {
   
    constructor(isHealthy) {
        super(isHealthy)
        
    }
    
    heal (name) {
      
        return name.isHealthy = true;
    }
        
}

class Hunter extends Traveler {

     constructor(isHealthy, food) {
        super(isHealthy, food)
        
        this.food = 2;
    }

    

    hunt() {
        this.food += 5;
    }

    eat() {
       
        this.food = this.food - 2;
    
        if(this.food <= 0){
           this.food = 0
           this.isHealthy = false;
       }
    }

    giveFood(name, numOfFoodUnits) {

        if(numOfFoodUnits <= this.food) {
            this.food -= numOfFoodUnits
           return name.food += numOfFoodUnits
        }
    }
    
}


class Wagon {
    constructor (capacity) {

        this._capacity = capacity;
        this._passengers = [];
    }

    get capacity () {
        return this._capacity
    }

    set capacity(newCapacity) {
        this._capacity = newCapacity
    }

    get passengers () {
       return this._passengers
    }

    set passengers (newPassenger) {
        return this._passengers = newPassenger
    }


    getAvailableSeatCount() {
        return this.capacity - this.passengers.length
    }

    join(traveler) {

        let hasRoom = this.capacity - this.passengers.length
        
        if(hasRoom > 0){
            this.passengers.push(traveler)
        }
    }

    shouldQuarantine() {

        for(let i in this.passengers) {

            if(this.passengers[i].isHealthy === false) {
                return true;
            }
        }

        return false;
    }

    totalFood() {

        let totalFood = 0;

        for(let i in this.passengers) {
            
            totalFood += this.passengers[i].food
        }

        return totalFood
    }

}



//   TESTES

// Cria uma carroça que comporta 4 pessoas
let wagon = new Wagon(4);
// Cria cinco viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');

console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);

wagon.join(maude); // Não tem espaço para ela!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);

sarahunter.hunt(); // pega mais 5 comidas
drsmith.hunt();

console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);

henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // juan agora está doente (sick)

console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);

drsmith.heal(juan);
console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);

sarahunter.giveFood(juan, 4);
sarahunter.eat(); // Ela só tem um, então ela come e fica doente

console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);